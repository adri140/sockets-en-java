package ServerManager;

import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Semaphore;
import java.net.ServerSocket;

public class ServerManager extends Thread {
	
	private static ServerManager server;
	
	public static ServerManager singelton()
	{
		if(server == null)
		{
			server = new ServerManager();
		}
		return server;
	}
	
	private final int port = 45000;
	private ServerSocket serverSocket;
	private boolean run;
	
	private List<PetitionController> controllers; 
	private Semaphore mutexController;
	
	public ServerManager()
	{
		controllers = new ArrayList<PetitionController>();
		mutexController = new Semaphore(1, true);
		try
		{
			this.serverSocket = new ServerSocket(port);
			run = true;
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
	}
	
	public void offServer()
	{
		this.run = false;
	}
	
	@Override
	public void run()
	{
		System.out.println("Servidor corriendo");
		try
		{
			serverSocket.setSoTimeout(10000);
			do {
				try {
					System.out.println("Esperando conexion");
					Socket conexion = serverSocket.accept();
					
					PetitionController peticion = new PetitionController(conexion);
					peticion.start();
					
					mutexController.acquire();
					controllers.add(peticion);
					mutexController.release();
				}
				catch(Exception e)
				{
					System.err.println("Error en el Server Manager: " + e.getMessage());
				}
			}while(run);
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
    }

	
}
