package ServerManager;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.net.*;

import aplication.Data;

public class PetitionController extends Thread {
	
	private final Socket peticion;
	
	public PetitionController(Socket peticion)
	{
		System.out.println("instanciado el peticion controller");
		this.peticion = peticion;
	}
	
	@Override
	public void run()
	{
		System.out.println("Peticion controller, iniciando servicios");
		try
		{
			//iniciamos el flujo de datos
			InputStreamReader dis = new InputStreamReader( peticion.getInputStream() );
			BufferedReader br = new BufferedReader(dis);
			
	        // Obtenemos el nombre del archivo
	        String nombreArchivo = br.readLine();
	        System.out.println(nombreArchivo);
	        
	        //dis.close(); //cierro la conexion	        
	        
	        //flujo de entrada para el arxivo
	        File file = new File("d:\\data\\server\\" + nombreArchivo);
	        
            
            if(file.exists())
            {
            	FileInputStream fos = new FileInputStream( file );
                BufferedInputStream in = new BufferedInputStream( fos );
                System.out.println("Obteniendo fixero");
	            
            	InetAddress direccion = peticion.getInetAddress();
	            DatagramSocket enviador = new DatagramSocket(45001);
                
                Long tam = file.length(); //cambiar, generar una lista de arrays de bytes, los dos primeros usar-los para indicar el orden, antes de esto enviar el tamanyo del fixero
	            System.out.println("tamaño archivo " + (int) (tam + 1));
                                
                long iteracionesnecesarias = tam / Data.BODY_SIZE + 1;
                
                System.out.println("iterar " + iteracionesnecesarias + " veces");
                Thread.sleep(50);
                for (long it = 0; it < iteracionesnecesarias; it++) {
                	byte[] datosleidos = new byte[Data.BODY_SIZE];
                	
                	in.read(datosleidos);
                	
                	boolean esUltimo = (it == iteracionesnecesarias - 1);
                	
                	Data datos = new Data((int) it, esUltimo, datosleidos);
                	byte[] paquete = datos.cogerDatos();
                	
                	DatagramPacket d = new DatagramPacket(paquete, paquete.length, direccion, 45001);
                	if(datos.getOrder() % 2 == 0) System.out.println("enviando paquete: " + datos.getOrder() + (esUltimo ? " el ultimo" : ""));
	            	enviador.send(d);
	            	Thread.sleep(30);
                }
                enviador.close();
                in.close();
                fos.close();
                System.out.println("Acabando de enviar");
                enviador.close();
                
                /*
	            while(tam != 0) {
	            	System.out.println(tam);
	            	if(tam >= 250) {
		            	tam = tam - 250;
		            	archivo.push(new byte[250]); //cambiar
	            	}
	            	else {
	            		
	            		for(int i = 0; i < tam + 1; i++)
	            		{
	            			archivo.push(new byte[(int) (tam + 0)]);
	            		}
	            		tam = tam - tam;
	            	}
	            }
	            
	            System.out.println("preparado");
	            Deque<Data> enviar = new ArrayDeque<Data>();
	            
	            
	            
	            for(int i = 0; i <archivo.size() && ntam != 0; i++) {
	            	
	            	
	            	byte[] array = archivo.poll();
	            	
	            	if(ntam >= 250)
	            	{
	            		for(int p = 0; p < 250; p++) {
	            			int y = in.read();
	            			array[p] = (byte) (y + 0);
	            		}
	            		ntam = ntam - 250;
	            	}
	            	else {
	            		for(int p = 0; p < ntam; p++) {
	            			int y = in.read();
	            			array[p] = (byte) (y + 0);
	            		}
	            		ntam = ntam - ntam;
	            	}
	            	enviar.push(new Data());
	            	
	            }
	            in.close();
	            fos.close();
	            
	            System.out.println("Arxivo obtenido, total: " + enviar.size());
	            System.out.println(peticion.getInetAddress());
	            //creo un flujo de salida para el array de bytes por UDP
	            InetAddress direccion = peticion.getInetAddress();
	            DatagramSocket enviador = new DatagramSocket(45001);
	            
	            for(byte[] b : enviar) {
	            	DatagramPacket d = new DatagramPacket(b, b.length, direccion, 45001);
	            	System.out.println("enviando paquete: " + b[0]);
	            	enviador.send(d);
	            }
	            enviador.close();
	            */
            }
            else
            {
            	System.err.println("El archivo pedido no existe");
            }
		}
		catch(Exception e)
		{
			e.printStackTrace();
		}
		
		System.out.println("Muere");
    }
}
