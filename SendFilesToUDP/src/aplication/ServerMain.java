package aplication;

import java.util.Scanner;

import ServerManager.ServerManager;

public class ServerMain
{
	
	private static ServerManager manager;
	
	public static void main(String[] args)
	{
		boolean run = true;
		System.out.println("Iniciando el servidor");
		runServer();
		//ByteArrayOutputStream
		Scanner reader = new Scanner(System.in);
		System.out.println("Iniciando la console");
		do
		{
			String command = reader.nextLine();
			
			switch(command)
			{
			case "stopServer":
				manager.offServer();
				run = false;
				break;
			case "description":
				break;
			}
			
		}while(run);
		reader.close();
	}
	
	private static void runServer()
	{
		manager = ServerManager.singelton();
		System.out.println("Instanciado el servidor");
		manager.start();
	}
	
}
