package aplication;

import java.util.Collections;
import java.util.List;

public class Helper
{
	public static byte[] getBytesFromInt(int a)
	{
		byte[] bytes = new byte[4];
		
		for (int i = 0; i < bytes.length; i++) {
			bytes[bytes.length - i - 1] = (byte) (a % Byte.MAX_VALUE);
			a = a / Byte.MAX_VALUE;
		}
		return bytes;
	}
	
	public static int buildIntFromBytes(byte[] bytes)
	{
		if(bytes.length != 4) throw new IllegalArgumentException("Longitud debe ser 4 y es " + bytes.length);
		
		int a = 0;
		for (int i = 0; i < bytes.length; i++) {
			a = a + bytes[i];
			if (i < bytes.length - 1) {
				a = a << 8;
			}
		}
		return a;
	}
	
	public static boolean tenemosTodosLosPaquetes(List<Data> paquetes)
	{
		Collections.sort(paquetes);
		int ultimoIndice = paquetes.size() - 1;
		Data ultimo = paquetes.get(ultimoIndice);
		int tamañoEsperado = ultimo.getOrder() + 1;
		
		return (ultimo.esUltimo() && tamañoEsperado == paquetes.size());
	}
}
