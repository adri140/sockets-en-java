package aplication;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class ClientMain {

	private static int port = 45000;
	private static String ip = "10.1.6.83";
	
	public static void main(String[] args)
	{
		try {
			
			Scanner sc = new Scanner(System.in);
			InetAddress ipAddress = InetAddress.getByName(ip);
		
			Socket connection = new Socket(ipAddress, port);
			
			PrintWriter txStream = new PrintWriter(connection.getOutputStream(),
					true);

				
				System.out.print("Escriu el nom d'un ficher: ");
				String tx = sc.nextLine();
				
				txStream.println(tx);
				
				System.out.println("enviado");
				
				txStream.close();
				
				DatagramSocket recibir = new DatagramSocket(45001);
				
				//List<byte[]> file = new ArrayList<byte[]>();
	            
				List<Data> paquetesrecibidos = new ArrayList<Data>();
				
				do {
					System.out.println("voy a recibir");
					byte[] bytesrecibidos = new byte[Data.HEAD_SIZE + Data.BODY_SIZE];
					DatagramPacket packet = new DatagramPacket(bytesrecibidos, bytesrecibidos.length);
					recibir.receive(packet);
					bytesrecibidos = packet.getData();
					
					Data datos = new Data(bytesrecibidos);
					
					System.out.println("paquete recibido: " + datos.getOrder());
					
					paquetesrecibidos.add(datos);
					
				} while(!Helper.tenemosTodosLosPaquetes(paquetesrecibidos));
				
				recibir.close();
				/*
				for(byte[] b : file)
				{
					DatagramPacket packet = new DatagramPacket(b, Integer.MAX_VALUE - 8);
					recibir.receive(packet);
					b = packet.getData();
					System.out.println("paquete recivido: " + b[0]);
				}
				
				recibir.close();
				*/
				//no se ordenan los paquetes jajajajaja
				
				//escribimos en el equipo
				
				File f = new File("d:\\data\\" + tx);
				//f.mkdir();
				f.createNewFile();
				
				FileOutputStream fos = new FileOutputStream(f);
				BufferedOutputStream bos = new BufferedOutputStream(fos);
				/*
				for(byte[] b : file)
				{
					bos.write(b);
				}
				*/
				
				for (Data d : paquetesrecibidos) {
					bos.write(d.getCuerpo());
				}
				bos.flush();
				bos.close();
				fos.close();
				
				System.out.println("recibido todo");
		}
		catch(Exception e) {
			e.printStackTrace();
		}
	}
	
}
