package aplication;

import java.util.Arrays;

public class Data implements Comparable<Data>
{
	public static int HEAD_SIZE = 5;
	public static int BODY_SIZE = Byte.MAX_VALUE * 32;
	
	private byte[] bytes = new byte[HEAD_SIZE + BODY_SIZE];
	
	
	private int order = -1;
	private boolean esUltimo = false;
	private byte[] cuerpo = new byte[BODY_SIZE];
	
	public Data(int orden, boolean ultimo, byte[] cuerpo)
	{
		this.setOrder(orden);
		this.marcarUltimo(ultimo);
		this.meterCuerpo(cuerpo);
	}
	
	public Data(byte[] todo)
	{
		this.bytes = todo;
				
		byte[] orderbytes = Arrays.copyOf(todo, 4);
		this.setOrder(orderbytes);
		
		this.marcarUltimo(bytes[4]);
		
		this.meterCuerpo(Arrays.copyOfRange(todo, HEAD_SIZE, HEAD_SIZE + BODY_SIZE));
	}
	
	public void setOrder(int order)
	{
		byte[] orderbytes = Helper.getBytesFromInt(order);
		
		for (int i = 0; i < orderbytes.length; i++) {
			bytes[i] = orderbytes[i];
		}
		
		this.order = order;
	}
	
	private void setOrder(byte[] bytes)
	{
		this.order = Helper.buildIntFromBytes(bytes);
	}
	
	public void marcarUltimo(boolean ultimo)
	{
		if (ultimo) {
			bytes[4] = Byte.MAX_VALUE;
		} else {
			bytes[4] = 0;
		}
		
		esUltimo = ultimo;
	}
	
	private void marcarUltimo(byte marca)
	{
		if (marca == Byte.MAX_VALUE) {
			this.esUltimo = true;
		} else {
			this.esUltimo = false;
		}
	}
	
	public void meterCuerpo(byte[] cuerpo)
	{
		if(cuerpo.length > BODY_SIZE) throw new IllegalArgumentException("Longitud del cuerpo mayor de 250. Era de " + cuerpo.length);
		
		for (int i = 0; i < cuerpo.length; i++) {
			bytes[i + HEAD_SIZE] = cuerpo[i];
		}
		
		this.cuerpo = cuerpo;
	}
	
	public byte[] cogerDatos()
	{
		return bytes;
	}
	
	public int getOrder()
	{
		return order;
	}
	
	public boolean esUltimo()
	{
		return esUltimo;
	}
	
	public byte[] getCuerpo()
	{
		return this.cuerpo;
	}

	@Override
	public int compareTo(Data o)
	{
		return this.order - o.getOrder();
	}
}
